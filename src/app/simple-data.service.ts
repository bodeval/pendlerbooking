import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Headers, Http } from '@angular/http';
import { Harbor, Route, UnitType, PersonType, Departure } from './models';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { DatePipe } from '@angular/common';
import { forEach } from '@angular/router/src/utils/collection';

@Injectable()
export class SimpleDataService {
   
  ruter:string
  
  base_url:string
  site_id:number
  mode:string
  
  constructor(private http: Http,  private datePipe: DatePipe) {

    // This variable 'rootVar' is set in index.html to get variables from outside in.
    let rootVar = window["rootVar"];    
    this.ruter =  rootVar.ruter
    this.base_url = rootVar.base_url
    this.loadingError(this.ruter, this.base_url)  
    this.mode =  rootVar.mode 

   }

   getScriptmanagerErrors(): Promise<any> {        
    return this.http.get(this.base_url +'/api/Diverse/ScriptManagerErrors')
               .toPromise()
               .then(response => 
                response.json() as any )
               .catch(this.handleError);
  }

   loadingError(ruter, base_url) {
      if(ruter == '') {
        
          alert("Fejl: Manglende opsætning af ruter i site settings")
        
        
      }

      if(base_url == '') {
        alert("Fejl: manglende opsætning af api addresse i site settings")
      }
   }

   setSite_id(site_id) {
     this.site_id = site_id
     
   }

  routes: Route[];


  getHarborsInPeriode(start_date, end_date): Promise<Route[]> {
    let str_start_date = this.datePipe.transform(start_date, 'dd-MM-yyyy');
    let str_end_date = this.datePipe.transform(end_date, 'dd-MM-yyyy');
    return this.http.get(this.base_url +'/api/Pendlerbooking/GetHarborsInPeriode/?site_id=' + this.site_id + '&start_date=' +str_start_date +'&end_date=' + str_end_date + '&ruter=' + this.ruter )
               .toPromise()
               .then(response => response.json() as any[])
               .catch(this.handleError);
  }


  getRoutes(start_date, end_date): Promise<Route[]> {
    let str_start_date = this.datePipe.transform(start_date, 'dd-MM-yyyy');
    let str_end_date = this.datePipe.transform(end_date, 'dd-MM-yyyy');
    return this.http.get(this.base_url +'/api/Pendlerbooking/GetHarbors/?site_id=' + this.site_id + '&start_date=' +str_start_date +'&end_date=' + str_end_date + '&ruter=' + this.ruter )
               .toPromise()
               .then(response => response.json() as Route[])
               .catch(this.handleError);
  }

  getUnitTypes(site_id): Promise<UnitType[]> {        
    return this.http.get(this.base_url +'/api/Pendlerbooking/GetUnitTypes/?site_id=' + site_id)
               .toPromise()
               .then(response => 
                response.json() as UnitType[] )
               .catch(this.handleError);
  }

  getPersonTypes(site_id): Promise<PersonType[]> {    
    return this.http.get(this.base_url +'/api/Pendlerbooking/GetPersonTypes/?site_id=' + site_id)
               .toPromise()
               .then(response => response.json() as PersonType[])
               .catch(this.handleError);
  }

  getSiteId(site_name): Promise<any> {    
    return this.http.get(this.base_url +'/api/Pendlerbooking/GetSiteIdBySiteName/?site_name=' + site_name)
               .toPromise()
               .then(response => response.json() as any)
               .catch(this.handleError);
  }

  sendToHub(message): Promise<any> {  

    return this.http.post('http://localhost:63081/api/Message', message
    )
         .toPromise()
         .then(response => 
           {
             console.log(message)
             return message
            })
         .catch(this.handleError);

  }


  getDepartures(options): Promise<Departure[]> {   
    
    
    options.str_start_date = this.datePipe.transform(options.StartDate, 'dd-MM-yyyy');
    options.str_end_date = this.datePipe.transform(options.EndDate, 'dd-MM-yyyy');
    options.site_id = this.site_id; 
    options.NoOfPersons = 0;
    options.NoOfAdults = 0;
    options.NoOfChildren = 0;
    options.NoOfIslanders = 0;
    /*
    voksen = 1
    barn = 2
    øbo = 3
    */
   // calculation number of persons and meta_persons
   
    options.PersonTypes.forEach(element =>{  
      
      if(element.number_of_persons > 0) {
        options.NoOfPersons += element.number_of_persons;
        
        switch(element.meta_person_type_id) {
          case 1:            
            options.NoOfAdults =+  element.number_of_persons;
            break;
          case 2:
            options.NoOfChildren +=  element.number_of_persons;
            break;
          case 3:
            options.NoOfIslanders +=  element.number_of_persons;
            break;
        }     
      }
      
      
    });
    
    return this.http.post(this.base_url + '/api/Pendlerbooking/GetDepartures', options
          )
               .toPromise()
               .then(response => response.json() as any[])
               .catch(this.handleError);
  }


  book(options): Promise<any> { 

    let person_types = []

    options.PersonTypes.forEach(element => {
      if(element.number_of_persons > 0) {
        person_types.push({Id: element.id, Amount: element.number_of_persons })
      }        
    });


    let booking =  {
      PortalId:0,
      DepartureIds: options.departure_ids, 
      ReturnDepartureIds: options.return_departure_ids, 
      PersonTypes : person_types,
      UnitType: {Id:options.UnitTypeId},
      ReturnUrl: '',
      Height: options.Height,             
      Length: options.Length,             
      NoOfAdults: options.NoOfAdults,       
      NoOfChildren: options.NoOfChildren,      
      NoOfIslanders: options.NoOfIslanders,  
      NoOfPersons: options.NoOfPersons,      
      NoOfUnits: options.NoOfUnits,      
      NumberOfItems: options.NumberOfItems,      
      RegID: options.RegID,      
      RouteId: options.RouteId,
      StartDate: options.StartDate,
      Weight: options.Weight,      
      WeightCargo: options.WeightCargo,
      site_id: options.site_id,
      Comment: '',
      CustomerName: options.CustomerName ,
      CustomerPhone: options.CustomerPhone,
     // extra_info_departures_html: options.extra_info_departures_html

      extra_departure_info: {
        "html": this.htmlEncode(options.extra_info_departures_html),
        "json": {origin: 'pendler'}
      },

      extra_return_departure_info: {
        "html": this.htmlEncode(options.extra_info_return_departures_html),
        "json": {origin: 'pendler'}
      }

    }

    return this.http.post(this.base_url + '/api/Pendlerbooking/Book', booking)
       .toPromise()
       .then(response => response.json() as any[])
       .catch(err => Promise.reject(err))
       //.catch(this.handleError);
  }


htmlEncode = function (value) {
   
    let entityMap = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      '"': '&quot;',
      "'": '&#39;',
      "/": '&#x2F;'
     };
     return String(value).replace(/[&<>"'\/]/g, s => entityMap[s]);
}


  checkDepartureAvailability(options): Promise<any[]> { 
    return this.http.post(this.base_url + '/api/Pendlerbooking/DepartureAvailability', options)
       .toPromise()
       .then(response => response.json() as any[])
       .catch(err => Promise.reject(err))
       //.catch(this.handleError);
  }

  getUserPages(site): Promise<any[]> { 
    return this.http.get(this.base_url + '/api/Diverse/GetUserPageBySiteId?SiteID='+ site)
       .toPromise()
       .then(response => response.json() as any[])
       .catch(err => Promise.reject(err))
       //.catch(this.handleError);
  }

  


  
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }


}

