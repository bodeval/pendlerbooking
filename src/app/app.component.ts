import { Component, OnInit } from '@angular/core';
import { SimpleDataService } from './simple-data.service';

import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Message } from './models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent  implements OnInit {

  hub_test_on:boolean = false
  title = 'Mine Sider';
  userpages:any;
  selected_item:string
  site_id:number;
  activeLink = 'bookings'
 
  // menu color
  background = 'primary';
  color="accent";
  mypage_is_active: boolean = false



  public hubConnection: HubConnection;
  public messages: Message[] = [];
  public message: Message;
  public message_txt: string;
  

  constructor(  private simpleDataService: SimpleDataService ) {

   }




  ngOnInit(): void {   
  
      let rootVar = window["rootVar"];
      this.simpleDataService.getSiteId(rootVar.site_name).then(result => {      
        this.site_id = result.selskabs_id
        rootVar.company_id = result.selskabs_id
        this.simpleDataService.setSite_id(this.site_id)
       
        if(this.mypage_is_active) {
          this.simpleDataService.getUserPages(rootVar.site_name).then(result => {
            this.userpages = result
        })
        }
      })



      if(this.hub_test_on) {
        // Signalr test
        let builder = new HubConnectionBuilder();
      
        // as per setup in the startup.cs
        //this.hubConnection = builder.withUrl('http://localhost:63081/hubs/echo').build();
        this.hubConnection = builder.withUrl('http://localhost:63081/notify').build();
        
    
        // message coming from the server
        this.hubConnection.on("Send", (message) => {
          this.messages.push(message);
        });
    
        // starting the connection
        this.hubConnection.start().then(() => console.log('Connection started!'))
        .catch(err => console.log('Error while establishing connection :('));

        this.hubConnection.on('BroadcastMessage', (type: string, payload: string) => {
          this.messages.push({ Type: type, Payload: payload });
        });
      }
     

  }

  send() {
    // message sent from the client to the server
    //this.hubConnection.invoke("Send", this.message_txt);
    this.message = {Type: 'error', Payload: this.message_txt};
    
  this.simpleDataService.sendToHub(this.message)
    this.messages.push(this.message)
    this.message_txt = ""
  }


  oldsend() {
    // message sent from the client to the server
    this.hubConnection.invoke("Send", this.message_txt);
    this.message = {Type: 'error', Payload: this.message_txt};
  }


  hasUserpage(page) {
    return this.userpages.filter(elm => elm.UserPageTypeDescription === page).length > 0
  }
}