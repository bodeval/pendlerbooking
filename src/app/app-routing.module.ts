import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { BookingsComponent } from './bookings/bookings.component';
import { AppComponent } from './app.component';
import { PendlerbookingComponent } from './pendlerbooking/pendlerbooking.component'
import { CustomerInfoComponentComponent } from './customer-info-component/customer-info-component.component';
import { CompumatCardComponent } from './compumat-card/compumat-card.component';


// const routes:Routes =  [
//   { path: 'errors', component: ScriptManagerErrorsComponent },
//   { path: 'pendlerbooking', component: PendlerbookingComponent },
//   { path: 'bookings', component: BookingsComponent },
//   { path: 'customer_info', component: CustomerInfoComponentComponent },  
//   { path: 'compumat_card', component: CompumatCardComponent },  
//   { path: '', redirectTo: '/bookings', pathMatch: 'full' }
// ];


const routes:Routes =  [
 
  { path: '', component: PendlerbookingComponent },
  { path: 'bookings', component: BookingsComponent },
  { path: 'customer_info', component: CustomerInfoComponentComponent }, 
  { path: '', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  exports: [RouterModule],
  imports: [ RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
