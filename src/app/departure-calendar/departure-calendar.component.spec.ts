import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartureCalendarComponent } from './departure-calendar.component';
import {} from 'jasmine';
describe('DepartureCalendarComponent', () => {
  let component: DepartureCalendarComponent;
  let fixture: ComponentFixture<DepartureCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartureCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartureCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
