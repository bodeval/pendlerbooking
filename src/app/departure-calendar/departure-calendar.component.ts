
import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { TEST_Departures} from '../test_departures'
import {Departure, Week, Day, WeekDayName, Times } from './calendar-models';
import { WeekPipe } from '../week.pipe';
import { DatePipe, CommonModule } from '@angular/common';
import { animate, trigger, transition, style } from '@angular/animations';

@Component({
  selector: 'app-departure-calendar',
  templateUrl: './departure-calendar.component.html',
  styleUrls: ['./departure-calendar.component.css'],
  animations: [
    trigger('stepAnimation', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))
      ]),
   transition(':leave', [  // before 2.1: transition('* => void', [
     style({transform: 'translateX(0%)'}),
     animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
   ])
  ])]
})



export class DepartureCalendarComponent implements OnInit, OnChanges {

  @Output() reloadDepartures = new EventEmitter<any>();
  
  add_to_booking:String = "Tilføj retur bookinger"
  go_to_booking:String = "Gå til booking"
  @Input() time:any
  @Input() step:number
  @Input() startHarbor
  @Input() endHarbor
  @Input() downloading  


  @Output() prepareReturnBooking = new EventEmitter<any>();

  @Output() bookDepartures = new EventEmitter<any>();  
  @Output() stepBack = new EventEmitter<number>();
  
  @Input()   departures:any[] = [];
  weeks:Week[] = []
  week_day_names:WeekDayName[] = []
  times = new Times()
  selected_hour:string
  selected_minute:string = "00"
  green:string = "rgb(187, 247, 187)"
  red:string = "pink"
  yellow:string = "rgb(247, 247, 100)"
  vis_afgange:boolean = false;
  return_booking:boolean = false;

  long_day_names = ['Mandag','Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag','Søndag']
  long_month_names = ['Januar','Februar','Marts','April','Maj','Juni','Juli','August', 'September', 'Oktober', 'November','December']
  selected_departures:Departure[] = []
  constructor(private week_pipe:WeekPipe, private datePipe: DatePipe ) { 
    this.long_day_names.forEach((element, index) => {      
      this.week_day_names.push(new WeekDayName(element, index < 5 ))    
    });
  }


  onPrepareReturnBookings() {
    this.return_booking = true
    this.prepareReturnBooking.emit(this.getSelectedDepartures());
  }

  onBook() {          
    this.bookDepartures.emit(this.getSelectedDepartures());        
  }

  onReloadDepartures() {    
    this.reloadDepartures.emit(
      //this.onTimeChange()
    )
    
  }

  onStepBack() {
    //this.step = 2
    this.stepBack.emit()
  }

  ngOnInit() {  }

  ngOnChanges() { 
    this.weeks = []
    this.getWeeks()
  }

  getDays():Day[] {
      let days:Day[] = []
      for(let i = 0; i < 7; i++) {
        days.push(new Day(i, this.week_day_names[i].checked))
      }          
    return days        
  }

  getDay(date):number {
    let day_number = date.getDay();
    if(day_number == 0) {
      day_number = 6
    } else {
      day_number -=1 
    }
    return day_number
  }

  getWeeks() {
    let i = -1;
    let week_number_counter = -1
    let date_counter = ""
    let day_selected = false;
    let month_counter = -1
    
   // TEST_Departures.forEach(element => {
    this.departures.forEach(element => {
      let date = new Date(element.DatoTid)
      let week_number = this.week_pipe.transform(date);      
      let str_date = this.datePipe.transform(date,'dd');
      let day_number = this.getDay(date)

      if(week_number != week_number_counter) {
        let week:Week = new Week(this.getDays(), week_number)        
        this.weeks.push(week)
        week_number_counter = week_number
        i += 1;
        this.weeks[i].month_number = date.getMonth()        
              if(this.weeks[i].month_number != month_counter) {
                this.weeks[i].is_first_week_in_month = true
                month_counter = this.weeks[i].month_number
              } else {
                this.weeks[i].is_first_week_in_month = false
              }
      }

      if(str_date != date_counter) {
        this.weeks[i].days[day_number].str_date = str_date
        date_counter = str_date
        day_selected = false
      }

      let d = new Departure();      
      
      this.weeks[i].days[day_number].departures.push(d)
      d.date = element.DatoTid;
      d.str_time = this.datePipe.transform(date,'HH:mm');
      d.sold_out = element.Udsolgt
      d.arrival_time = element.ArrivalTime
      d.travel_time = element.TravelTime
      d.path = element.path
      d.locked = element.Locked
      d.ferry_name = element.ferry_name

      if(d.str_time >= this.time.hour+':'+this.selected_minute  && !day_selected ) {
        this.weeks[i].days[day_number].time_match_count += 1
        if(!d.sold_out) {
          d.selected = true          
          day_selected = true;          
          this.weeks[i].days[day_number].match_color = this.weeks[i].days[day_number].time_match_count == 0 ? this.green : this.weeks[i].days[day_number].time_match_count == 1 ? this.yellow : this.red                    
          this.weeks[i].days[day_number].selected_departure = d
        }         
      }   
    })        
  }

  onWeekDayCheckChange(week_day, index) {
    let checked = week_day.checked 
    this.weeks.forEach(element => {
      element.days[index].checked = checked
    });
    this.onTimeChange()
  }

  onWeekCheckChange(week) {
    let checked = week.checked
    week.days.forEach(element => {
      element.checked = checked      
    });
    this.onTimeChange()
  }

  getSelectedDepartures() {
    let result:any[]=[]
    this.weeks.forEach(week => {
      week.days.forEach(day => {
        day.departures.forEach(element => {
          if(element.selected==true && day.checked==true) {
            result.push(element)
          }          
        });
      });      
    });
    this.selected_departures = result
    return result
  }

  onTimeChange() {
    if(this.time.hour != null && this.selected_minute != null && this.departures.length > -1) {     
      let day_selected = false;
      this.weeks.forEach(week => {
        
        week.days.forEach((day, index) => {
          day.selected_departure = null
          day.time_match_count = -1
          day.match_color = ""
          day_selected = false
          day.departures.forEach(departure => {
              if(!day_selected) {
                departure.selected = false          
              }
              
              if(departure.str_time >= this.time.hour+':'+this.selected_minute && !day_selected && this.week_day_names[index].checked && week.checked ) {
                day.time_match_count += 1
                if(!departure.sold_out && !departure.locked ) {
                  if(day.time_match_count == 0) {
                    departure.selected = true                          
                    day_selected = true
                    day.selected_departure = departure
                  }
                  
                  day.match_color = day.time_match_count == 0 ? this.green : day.time_match_count == 1 ? this.yellow : this.red                          
                }         
              }               
          }); 
         
            day.checked = day_selected          
        });
      });
    }        
    this.onReloadDepartures()
  }

  onDepartureChange(day) {
    day.match_color = this.green
    day.checked = true
    
    day.departures.forEach(element => {
      if(element==day.selected_departure) {
        element.selected = true
      } else {
        element.selected = false
      }      
    });
  }
}
