export class Week {
    constructor(public days:Day[], private week_number:number) {
        this.days = days
        this.week_number = week_number
    }
    checked:boolean = true  
    month_number:number
    is_first_week_in_month:boolean
}

export class WeekDayName {
    constructor(private name:string, public checked:boolean) { this.name = name; this.checked = checked; }    
}

export class Day {    
    departures:Departure[] = [];
    constructor(private day_number:number, public checked:boolean) {
        this.day_number = day_number;
        this.checked = checked
    }
    str_date:string
    time_match_count:number = -1   
    match_color:string 
    selected_departure:Departure
}

export class Departure {
    date: Date
    week: number
    str_time:string
    selected:boolean = false
    sold_out:boolean
    travel_time:number
    arrival_time:Date
    path: number[]   
    locked: boolean 
    ferry_name:string
  }

  export class Times {
    hours:string[] = []
    minutes:string[] = []
    constructor() {
        for(let i = 0; i < 24; i++) {
            this.hours.push(this._to2digit(i))
        }
        for(let i = 0; i < 60; i+=5) {
            this.minutes.push(this._to2digit(i))
        }
    }
    private _to2digit(n: number) {
        return ('00' + n).slice(-2);
    } 
  }

  