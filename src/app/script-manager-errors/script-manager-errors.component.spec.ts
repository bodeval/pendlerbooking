import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScriptManagerErrorsComponent } from './script-manager-errors.component';

describe('ScriptManagerErrorsComponent', () => {
  let component: ScriptManagerErrorsComponent;
  let fixture: ComponentFixture<ScriptManagerErrorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScriptManagerErrorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptManagerErrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
