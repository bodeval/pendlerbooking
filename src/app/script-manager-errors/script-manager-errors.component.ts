import { Component, OnInit } from '@angular/core';
import { SimpleDataService } from '../simple-data.service';

@Component({
  selector: 'app-script-manager-errors',
  templateUrl: './script-manager-errors.component.html',
  styleUrls: ['./script-manager-errors.component.css']
})
export class ScriptManagerErrorsComponent implements OnInit {

  errors:any
  count_site:any
  count_href:any

  count_errors:number
  count_last_hour:number
  count_one_day_ago:number
  count_two_day_ago: number
  count_last_24_hour: number


  constructor( private simpleDataService: SimpleDataService) { }

  ngOnInit() {

    this.simpleDataService.getScriptmanagerErrors().then(result => {  
      this.errors = result.errors
      this.count_site = result.count_site
      this.count_href = result.count_href

      this.count_errors = result.count_errors[0].Column1
      this.count_last_hour = result.count_last_hour[0].Column1
      this.count_one_day_ago = result.count_one_day_ago[0].Column1
      this.count_two_day_ago = result.count_two_day_ago[0].Column1
      this.count_last_24_hour = result.count_last_24_hour[0].Column1

    })
  }

}
