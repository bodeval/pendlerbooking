import { OPTIONS } from './options';
import { Component, OnInit } from '@angular/core';

import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

import {ChangeDetectionStrategy} from '@angular/core';

import { SimpleDataService } from '../simple-data.service';
import { Departure } from '../models';

@Component({
  selector: 'app-rand-pic',
  templateUrl: './rand-pic.component.html',
  styleUrls: ['./rand-pic.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RandPicComponent implements OnInit {

  options = OPTIONS
  test = []
  movies:any[] = []
  max_pics:number = 3
  departures:Departure[] = [];

  items = Array.from({length: 100000}).map((_, i) => `Item #${i}`);

  constructor(  private simpleDataService: SimpleDataService ) { 

    this.simpleDataService.setSite_id(2)
  }

  ngOnInit() {
    this.getPics()
    // let d = new Departure()
    // d.Navn = "test"
    // this.departures.push(d)
    // this.departures.push(d)
    // this.departures.push(d)
    this.getDepartures(this.options)


  }

  getDepartures(options) {  

    this.simpleDataService.getDepartures(options)
    .then(dep => {        
      this.departures = dep

    

       

     });
    
  }   

  counter(i: number) {
        return new Array(i);
  }

  drop(event: CdkDragDrop<string[]>) {
    
    moveItemInArray(this.movies, event.previousIndex, event.currentIndex);
  }

  getPics = function() {

    if(this.max_pics < 1) {
      this.max_pics = 0
    }

    let n:number = 0
    this.movies = []
    while (n < this.max_pics) {
      this.movies.push( "https://picsum.photos/200/300/?image=" + (n +1))
      n += 1
    }
  }

  parse = function(number_str) {
    var numberPattern = /\d+/g;

    
    return parseInt(number_str.match( numberPattern ),10) % 1000
  }
 
}
