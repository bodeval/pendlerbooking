import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RandPicComponent } from './rand-pic.component';

describe('RandPicComponent', () => {
  let component: RandPicComponent;
  let fixture: ComponentFixture<RandPicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RandPicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RandPicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
