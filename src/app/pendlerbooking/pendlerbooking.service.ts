import { Injectable } from '@angular/core';
import { ChangeTimeDialogComponent } from './change-time-dialog/change-time-dialog.component';
import { UnitType } from '../models';

@Injectable({
  providedIn: 'root'
})
export class PendlerbookingService {

  constructor() { }

  say():void {
    alert("hej")
  }



  initPersonAndUnitType(comp:any):void {
    comp.simpleDataService.getUnitTypes(comp.site_id).then(unit_types => {
      comp.unit_types = unit_types
      comp.downloading = false
      comp.simpleDataService.getPersonTypes(comp.site_id).then(
        person_types => {
          comp.person_types = person_types
          comp.pendlerbookingService.checkLocalStorage(comp)
        });       
    });

  }

  checkLocalStorage(comp:any):void {
    let params:any = comp.localStorage.getOnLocalStorage()    
    // if local storage is loaded
    if(params.params) {
      let unitTypeId = params.params.unit_type.Enhedstype_id;
     
      let storageUnitType = comp.unit_types.find(x=>x.Enhedstype_id == unitTypeId)
      if(storageUnitType) {
        // set selected unittype
        comp.selected_unit_type =  storageUnitType
        comp.select_unit_type()

        // set selected persontypes
        params.params.unit_type.person_types.forEach(elm => {          
          let pt = comp.person_types.find(x=>x.id == elm.id)          
          pt.number_of_persons = elm.number_of_persons
        });
      }
    }
  }



  getExtraInfoTH():string {

    let rows = ['Afgang', 'Kl.', 'Afgangsdato', 'Ankomst', 'Kl.', 'Ankomstdato', 'Færge']
    let result = ""
    rows.forEach(element => {
      result += '<th style="background-color: #eff0f1;text-align: left;border: 1px solid black;padding: 5px;">' + element +'</th>'
    });
    return '<tr>' + result + '</tr>'
  }


  getExtraInfo(element:any, comp:any):string {

    
    let result = ""
    let elements = [comp.selected_start_harbor.Navn, 
      comp.datePipe.transform(element.date, 'HH:mm'), 
      comp.getDayName(element.date) + ' d. ' + comp.datePipe.transform(element.date, 'dd-MM-yyyy'), 
      comp.selected_end_harbor.Navn,
      comp.datePipe.transform(element.arrival_time , 'HH:mm'),
      comp.getDayName(element.arrival_time) + ' d. ' + comp.datePipe.transform(element.arrival_time , 'dd-MM-yyyy'),
                    element.ferry_name ]

    elements.forEach(element => {
      result += '<td style="border: 1px solid black;padding: 5px;">' + element + '</td>'
    });
   
    return '<tr>' + result + '</tr>'
  }


  filter_unit_types(comp:any):void {
    if(!(comp.selected_start_harbor.havne_id && comp.selected_end_harbor.havne_id)) {
      comp.selected_unit_types = [];
      return
    } else {
      let u:UnitType[] = [];

      comp.unit_types.forEach(element => {
        let a = element.ferry_ids.split(",");
        if( a.indexOf(comp.selected_route.ferry_id+"") > - 1) {
          u.push(element)
        }
        comp.selected_unit_types = u;        
      });
    }
  }


}
