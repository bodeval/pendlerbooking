import { PendlerbookingService } from './pendlerbooking.service';
import { Component, OnInit } from '@angular/core';
import { SimpleDataService} from '../simple-data.service';

import { Harbor, Route, UnitType, PersonType, Departure, BookingOptions } from '../models';
import 'rxjs/add/operator/toPromise';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {FormControl, FormBuilder} from '@angular/forms';

import { animate, trigger, transition, style } from '@angular/animations';
import {LocaleStorageService } from '../locale-storage.service';
import {MatDialog} from '@angular/material';
import { DialogComponentComponent} from '../dialog-component/dialog-component.component';
import { ChangeTimeDialogComponent } from './change-time-dialog/change-time-dialog.component';
import { DatePipe } from '@angular/common';

// import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';  

@Component({
  selector: 'app-pendlerbooking',
  templateUrl: './pendlerbooking.component.html',
  styleUrls: ['./pendlerbooking.component.css'],
  
  animations: [
    trigger('stepAnimation', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))
      ]),
   transition(':leave', [  // before 2.1: transition('* => void', [
     style({transform: 'translateX(0%)'}),
     animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
   ])
  ])]
})
export class PendlerbookingComponent implements OnInit {

  // _hubConnection: HubConnection;

  mode:string = 'onlinebooking'
  daynames = ["Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag"]
  step = 0  
  title = 'Pendlerbooking';
  routes: Route[] = [];
  

  harbors: any[] = [];
  downloading:boolean = false;  
  unit_types: UnitType[] = [];
  person_types: PersonType[] = [];
  today:Date = new Date()
  startDate = new FormControl(new Date());
  endDate = new FormControl();
  last_day = this.today.addDays(290);
  options:BookingOptions = new BookingOptions();
  site_id:number
  customer_name:string
  customer_phone:string
  time = {hour: null, minute: "00"}

  minDate = new Date(this.today.getFullYear() , this.today.getMonth(), this.today.getUTCDate());
  maxDate = new Date(this.last_day.getFullYear() , this.last_day.getMonth(), this.last_day.getDate());
  endDateminDate = this.minDate.addDays(1);
  
  selected_route:Route = new Route();  
  selected_start_harbor:Harbor = new Harbor();
  selected_end_harbor:Harbor = new Harbor();
  selected_unit_type:UnitType = new UnitType();
  departures: Departure[] = [];
  selected_unit_types:UnitType[] = [];
  pendler_routes:string

  constructor(  
                private pendlerbookingService: PendlerbookingService,
                private simpleDataService: SimpleDataService,               
                private fb: FormBuilder,
                public dialog: MatDialog,
                private localStorage: LocaleStorageService,
                private datePipe: DatePipe                
              ) { }

  ngOnInit(): void {    
     this.startDate = new FormControl(new Date()); 
     this.downloading = true
     let rootVar = window["rootVar"];
     

     // configuration reading
     this.customer_name = rootVar.customer_name
     this.customer_phone = rootVar.customer_phone     
    
     if(rootVar.mode) {     
       this.mode = rootVar.mode;       
     }

     if(rootVar.ruter) {     
      this.pendler_routes = rootVar.ruter;      
     }

    
    
     this.options.mode = this.mode
     this.options.pendler_routes = rootVar.ruter
 
    if(this.simpleDataService.site_id != undefined) {
      this.site_id = this.simpleDataService.site_id        
      this.pendlerbookingService.initPersonAndUnitType(this)     

     } else {
       this.simpleDataService.getSiteId(rootVar.site_name).then(result => {      
        this.site_id = result.selskabs_id
        this.simpleDataService.setSite_id(this.site_id)
        this.pendlerbookingService.initPersonAndUnitType(this) 
      })
     }
  }

 
  prepareReturnBooking(departures) {
    // shift start and end harbor.
    let start_harbor = this.selected_start_harbor
    let end_harbor = this.selected_end_harbor

    let dialogRef = this.dialog.open(ChangeTimeDialogComponent, {      
     data: {start_harbor: end_harbor, end_harbor: start_harbor }      
    });

    dialogRef.afterClosed().subscribe(time_result => {
      if(time_result) {

        let result:any[] = []
        let extra_info_departures_html:string = ""
        departures.forEach(element => {       

          if(element.path.length > 0) {
            result.push(element.path)
            extra_info_departures_html += this.pendlerbookingService.getExtraInfo(element, this)            
          }
        });
        
        this.options.departure_ids = result

        this.time = time_result

        let t = '<h3>Rejseplan:' + start_harbor.Navn + ' - ' +  end_harbor.Navn +  '</h3>' 
            + '<br /><table style="font-size:12px;font-family: arial, verdana, sans-serif;width:100%;border-collapse: collapse;margin-top: 10px;" class="extra_departure_info tabel tabel-bordered">' 
            + this.pendlerbookingService.getExtraInfoTH()        
        
        this.options.extra_info_departures_html = '<div class="edpextracont">' + t + extra_info_departures_html + "</table></div>"

        this.selected_start_harbor = end_harbor
        this.selected_end_harbor = start_harbor
        this.onOptionsSubmit(this.options)
      }
    });
  }

  getDayName(date) {
    return this.daynames[new Date(date).getDay()]
  }

  bookDepartures(departures) {
    this.downloading = true
    this.options.CustomerName = this.customer_name
    this.options.CustomerPhone = this.customer_phone
    let result:any[] = []

    let extra_info_departures_html:string = ""
    let start_harbor = this.selected_start_harbor
    let end_harbor = this.selected_end_harbor
    departures.forEach(element => {        
        if(element.path.length > 0) {
          result.push(element.path)
          extra_info_departures_html += this.pendlerbookingService.getExtraInfo(element, this)    
        }
    });
    
    // if there is departures this result is return departures
    if(this.options.departure_ids) {
      this.options.return_departure_ids = result;
    } else {
      this.options.departure_ids = result
    }
    

    let header_text = "Rejseplan: " + start_harbor.Navn + " - " + end_harbor.Navn

    // if Retur bookings
    let is_return_booking = this.options.extra_info_departures_html.length > 0
    if(is_return_booking) {
      header_text = "Rejseplan Retur: " + start_harbor.Navn + " - " + end_harbor.Navn
    }

    
    let t = '<h3>' + header_text 
          + '</h3><table style="font-size:12px;font-family: arial, verdana, sans-serif;width:100%;border-collapse: collapse;margin-top: 10px;" class="extra_departure_info tabel tabel-bordered">' 
          + this.pendlerbookingService.getExtraInfoTH()    
    
    if(is_return_booking) {
      this.options.extra_info_return_departures_html = '<div class="edpextracont">' +  t + extra_info_departures_html + "</table></div>"
    } else {
      this.options.extra_info_departures_html = '<div class="edpextracont">' + t + extra_info_departures_html + "</table></div>"
    }
    
    this.simpleDataService.book(this.options).then(result => {   
      document.location.href = result.Uri       
    }).catch(err => {
        
       let error_deps = JSON.parse(err._body)
       this.options.str_departure_ids = error_deps.NotBooked.join()

        this.simpleDataService.checkDepartureAvailability(this.options).then(
          // then confirm du u wish to booke without not available departure ??????
          result => {
           
            let dep = []
            result.forEach(element => {
              let msg = []
              if(element.number_of_persons_ok * element.weight_ok * element.length_ok == 0 ) {
                if(element.number_of_persons_ok == 0) {
                  msg.push("Der er ikke plads til " + this.options.NoOfPersons + " passagerer")
                }

                if(element.weight_ok == 0) {
                  msg.push("Der er ikke plads til " + (this.options.WeightCargo +  this.options.WeightCargo) + " kg. ekstra")
                }

                if(element.length_ok == 0) {
                  msg.push("Der er ikke plads på vogndækket. " + this.options.NoOfPersons + " meter er for langt. ")
                }
                dep.push({id: element.Afgangs_id , name: element.Navn, tid: element.DatoTid, messages: msg})
              }              
            });

            if(dep.length > 0) {
              let dialogRef = this.dialog.open(DialogComponentComponent, {
                width: '650px',
                height: '400px',
                data: dep
              });
          
              dialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed');
                this.onReloadDepartures()
              });
            }

        })
    })
  }

  onDownloading(val) {
    this.downloading = val
  }
  onReloadDepartures() {    
    this.onOptionsSubmit(this.options)
  }

/***************************************SELECT DATES************************************************** */
  selectStartDate(type: string, event: MatDatepickerInputEvent<Date>) {
    this.endDateminDate = new Date(event.value.getFullYear() , event.value.getMonth(), event.value.getDate()).addDays(1);    
    if(this.endDate.value) {
      if(this.endDate.value < this.startDate.value) {
        this.endDate.setValue(null);        
      }
    }

    if(this.step > 2) {
      this.onReloadDepartures()
    }    
  }
  /***************************************SELECT DATES END********************************************** */

  selectEndDate(type: string, event: MatDatepickerInputEvent<Date>) {  
    if(this.mode == 'compuferry_admin'){
      
      this.getHarbors()
    } else {
      this.getRoutes()   
    }
    
    
   
    if(this.step==0) {
      this.step  = 1
    }
    if(this.step > 2) {
      this.onReloadDepartures()
    } 
  }

  getDepartures(options):void {  

    options.mode = this.mode
    options.pendler_routes = this.pendler_routes
    this.downloading = true
    //this.departures = [];  
     this.simpleDataService.getDepartures(options)
     .then(departures => {        
       this.departures = departures
       this.downloading = false
       this.step = 3
      });
   }
  
  getRoutes(): void {    
    this.simpleDataService.getRoutes(this.startDate.value, this.endDate.value).then(routes => this.routes = routes);
  }

  getHarbors(): void {   
    this.simpleDataService.getHarborsInPeriode(this.startDate.value, this.endDate.value).then(harbors => {
      
      this.harbors = harbors
    });
  }

 
  select_start_harbor(route, harbor): void {    
        
    // if onlinebooking
    if(route) {      
      if(route.id != this.selected_route.id) {
        this.selected_end_harbor = new Harbor();
      }    
      this.selected_route = route;    
      this.pendlerbookingService.filter_unit_types(this)
    } else {
      
      this.selected_unit_types = this.unit_types
    }
    
    this.selected_start_harbor = harbor;

    
    if(this.selected_start_harbor == this.selected_end_harbor) {
      this.selected_end_harbor = new Harbor();
    }
    
  }

  select_end_harbor(route, harbor): void {
   
      // if onlinebooking
      if(route) {  
        if(route.id != this.selected_route.id) {
          this.selected_start_harbor = new Harbor();
        }    
        this.selected_route = route;
        this.pendlerbookingService.filter_unit_types(this)
      } else {
        
        this.selected_unit_types = this.unit_types
      }
    this.selected_end_harbor = harbor;   

    if(this.selected_start_harbor == this.selected_end_harbor) {
      this.selected_start_harbor = new Harbor();
    }    
  }

  onOptionsSubmit(options) {
    this.options = options
    this.options.StartDate = this.startDate.value
    this.options.EndDate =  this.endDate.value
    this.options.RouteId =  this.selected_route.id
    this.options.DepartureHarborId = this.selected_start_harbor.havne_id
    this.options.ArrivalHarborId = this.selected_end_harbor.havne_id
    this.options.UnitTypeId = this.selected_unit_type.Enhedstype_id
    this.getDepartures(options);    
  }

  select_unit_type() {
    this.step = 2    
    this.options = new BookingOptions() 
  }

  stepBack() {
    this.step = 2
  }
  
}


