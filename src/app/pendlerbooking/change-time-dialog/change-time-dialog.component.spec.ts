import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeTimeDialogComponent } from './change-time-dialog.component';

describe('ChangeTimeDialogComponent', () => {
  let component: ChangeTimeDialogComponent;
  let fixture: ComponentFixture<ChangeTimeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeTimeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeTimeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
