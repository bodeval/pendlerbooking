import { Component, OnInit, Inject } from '@angular/core';
import { Times } from '../../departure-calendar/calendar-models';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-change-time-dialog',
  templateUrl: './change-time-dialog.component.html',
  styleUrls: ['./change-time-dialog.component.css']
})
export class ChangeTimeDialogComponent implements OnInit {
  time = {hour: null, minute: "00"}
  times = new Times()
  constructor(

    public dialogRef: MatDialogRef<ChangeTimeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public harbors: any
  ) { }

  ngOnInit() {
  }

  onTimeChange() {
    //alert("hej")
  }


}
