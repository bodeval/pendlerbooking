import { TestBed, inject } from '@angular/core/testing';

import { PendlerbookingService } from './pendlerbooking.service';

describe('PendlerbookingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PendlerbookingService]
    });
  });

  it('should be created', inject([PendlerbookingService], (service: PendlerbookingService) => {
    expect(service).toBeTruthy();
  }));
});
