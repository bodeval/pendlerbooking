import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {} from 'jasmine';
import { PendlerbookingComponent } from './pendlerbooking.component';

describe('PendlerbookingComponent', () => {
  let component: PendlerbookingComponent;
  let fixture: ComponentFixture<PendlerbookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendlerbookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendlerbookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
