export interface  Message {
  Type: string;
  Payload: string 

}

export class BookingOptions {

  BookingCustomerType: string;
  CustomerID: number;
  Departures: Departure[];
  NoOfAdults: number;
  NoOfChildren: number;
  NoOfIslanders: number;
  NoOfPersons: number;
  NoOfUnits: number;
  NumberOfItems: number = 1;
  PhoneNo: string;
  RegID: string;
  SiteID: string;
  site_id: number;
  UnitTypeID: number;
  UnitTypeName: string
  UserID: number;
  Length: number;
  Weight: number;  
  Height: number;
  WeightCargo: number;
  WeightTotal: number;
  DangerousGoods: boolean;
  Comment: string;
  BookingOrigin: string;
  PersonTypes: PersonType[];
  StartDate: Date;
  EndDate: Date;
  ArrivalHarborId: number;
  DepartureHarborId: number;
  UnitTypeId: number;
  RouteId: number;
  str_start_date: string;
  str_end_date: string;
  is_set:boolean = false;
  departure_ids:any[]
  return_departure_ids:any[]
  str_departure_ids: string
  str_return_departure_ids: string
  CustomerName: string
  CustomerPhone: string
  extra_info_departures_html: string = ""
  extra_info_return_departures_html: string = ""
  mode: string
  pendler_routes: string



}


export class Harbor {
  havne_id: number;
  Navn: string;
  Selskabs_id: number;
  Latitude: string;
  Longitude: string;
  initialer?: any;
}

export class Route {
  id: number;
  name: string;
  ferry_name: string;
  ferry_id: number;
  harbors: Harbor[];
}

export class UnitType {
  Enhedstype_id: number;
  Navn: string;
  EnhStor: number;
  StdVagt: number;
  StdLangde: number;
  BookSelv: boolean;
  Hoj: boolean;
  KravAntalPersoner: boolean;
  KravVagt: boolean;
  KravFragtVaegt: boolean;
  KravLangde: boolean;
  Beskrivelse: string;
  KravFarligtGods: boolean;
  Sortering: number;
  C_varetekst: string;
  selskabs_id: number;
  AngivRegId: boolean;
  AngivAntal: boolean;
  Kundetyper: string;
  TilladOpsplitning: boolean;
  ItemNumbers: string;
  Touchable: boolean;
  active: boolean;
  KravStk: boolean;
  KravFragtVaegtRetur: boolean;
  KravVaegtRetur: boolean;
  KravMinimumEnPerson?: boolean;
  person_type_ids: string;
  ferry_ids: string;
  statistik_type_id?: number;
  limits?: any;
  person_types?: PersonType[];
}

export class PersonType {
  id: number;
  name: string;
  company_id: number;
  c_varetekst: string;
  position: number;
  active: boolean;
  created_at: string;
  updated_at: string;
  touchable: boolean;
  ferrys: string;
  customer_types: string;
  meta_person_type_id: number;
  unit_type_ids: string;
  number_of_persons: number;
}


export  class Departure {
  Overfartstid: number;
  Afgangs_id: number;
  DatoTid: string;
  Navn: string;
  Overfart_id: number;
  Rute_Id: number;
  starthavn?: any;
  sluthavn?: any;
  Fartoj_id: number;
  starthavn_id: number;
  sluthavn_id: number;
  ferry_name: any;
  BookSelvProcent: number;
  EnhedsTypeBookSelvPct: number;
  PersonBookSelvPct: number;
  tonnage?: any;
  Akt_lukketid_IfBookings?: any;
  Udsolgt: boolean;
  Locked: boolean;
  Akt_lukketid: number;
  MaxLength: number;
  MaxHeight: number;
  MaxUnitsAmount: number;
  MaxPers: number;
  MaxWeight: number;
  SumWeight: number;
  SumPersons: number;
  SumUnitsAmount: number;
  SumLength: number;
  SumAdults: number;
  SumChildren: number;
  SumIslanders: number;
  SumBookings: number;
  naeste: number;
  path: number[];
  isGood: boolean;
  isLast: boolean;
}


