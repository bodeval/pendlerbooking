import { BrowserModule } from '@angular/platform-browser';

import { StorageServiceModule } from 'angular-webstorage-service';


import { NgModule, LOCALE_ID } from '@angular/core';

import { AppComponent } from './app.component';

import { HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';





import { SimpleDataService} from './simple-data.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



import {
      MatButtonModule,
      MatCardModule,
      // MatMenuModule,
      MatToolbarModule,
      MatIconModule,
      MatDatepickerModule,
      MatFormFieldModule,
      MatNativeDateModule,
      MatInputModule,
      MAT_DATE_LOCALE,
      NativeDateAdapter,
      DateAdapter,
      MatSelectModule,
      MatCheckbox,
      MatRippleModule,
      MatTableModule,
      MatPaginatorModule,
      MatSortModule,
      MatDialogModule,
      MatSnackBar,
      MatSnackBarModule

} from '@angular/material';

import {} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';



import {MyDateAdapter} from './MyDateAdapter'


import { DatePipe } from '@angular/common';
import { ClickStopPropagation } from './stop-propagation.directive';
import { UnittypeFormComponent } from './unittype-form/unittype-form.component';
import { WeekPipe } from './week.pipe';
import {Safe } from './custom_pipes.pipe';
import { DepartureCalendarComponent } from './departure-calendar/departure-calendar.component';

import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { AppRoutingModule } from './app-routing.module';
import { BookingsComponent } from './bookings/bookings.component';
import { DialogComponentComponent } from './dialog-component/dialog-component.component';
import { PendlerbookingComponent } from './pendlerbooking/pendlerbooking.component';
import { BookingsTableComponent } from './bookings-table/bookings-table.component';
import { InfoDialogDialog } from './bookings-table/InfoDialogDialog';
import { EditBookingDialogComponent } from './bookings-table/edit-booking-dialog/edit-booking-dialog.component';
import { ScriptManagerErrorsComponent } from './script-manager-errors/script-manager-errors.component';


import { MatTooltipModule } from '@angular/material/tooltip';
import { RetBookingComponent } from './bookings-table/ret-booking/ret-booking.component';
import { CustomerInfoComponentComponent } from './customer-info-component/customer-info-component.component';
import { CompumatCardComponent } from './compumat-card/compumat-card.component';
import { ConfirmationDialog } from './bookings-table/ConfirmationDialog'
import { LocaleStorageService} from './locale-storage.service';
import { ChangeTimeDialogComponent } from './pendlerbooking/change-time-dialog/change-time-dialog.component';
import { RandPicComponent } from './rand-pic/rand-pic.component'

import {DragDropModule} from '@angular/cdk/drag-drop';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';





@NgModule({
  declarations: [
    AppComponent,
    ClickStopPropagation,
    UnittypeFormComponent,
    WeekPipe, Safe,
    DepartureCalendarComponent,
    BookingsComponent,
    DialogComponentComponent,
    PendlerbookingComponent,
    BookingsTableComponent,
    EditBookingDialogComponent,
    ScriptManagerErrorsComponent,
    InfoDialogDialog, RetBookingComponent, CustomerInfoComponentComponent, CompumatCardComponent, ConfirmationDialog, ChangeTimeDialogComponent, RandPicComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    // MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRippleModule,
    MatProgressBarModule,
    AppRoutingModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule ,
    MatTooltipModule,
    MatSnackBarModule,
    // MatTabsModule,
    StorageServiceModule,
    DragDropModule,
    ScrollDispatchModule


  ],


  //providers: [SimpleDataService,{provide: MAT_DATE_LOCALE, useValue: 'da-DK'}, {provide: DateAdapter, useClass: MyDateAdapter},],
  providers: [LocaleStorageService,
    WeekPipe,
    Safe,
    DatePipe,
    SimpleDataService,
    {provide: MAT_DATE_LOCALE, useValue: 'da-DA'},
    {provide: DateAdapter, useClass: MyDateAdapter},
],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponentComponent, EditBookingDialogComponent, InfoDialogDialog, RetBookingComponent, ConfirmationDialog, ChangeTimeDialogComponent]

})



export class AppModule { }

