import { NativeDateAdapter, DateAdapter  } from '@angular/material';


  
  export class MyDateAdapter extends NativeDateAdapter {
    
    MY_DATE_FORMATS = {
        parse: {
            dateInput: {month: 'short', year: 'numeric', day: 'numeric'}
        },
        display: {
            // dateInput: { month: 'short', year: 'numeric', day: 'numeric' },
            dateInput: 'input',
            monthYearLabel: {year: 'numeric', month: 'short'},
            dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
            monthYearA11yLabel: {year: 'numeric', month: 'long'},
        }
      };
    
  
      format(date: Date, displayFormat: Object): string {
        let long_names = ['Søndag','Mandag','Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag']
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        if (displayFormat == "input") {
            
            return long_names[date.getDay()] + ' ' + this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
        } else {
            //return date.toLocaleDateString();
            return long_names[date.getDay()] + ' ' + this._to2digit(day) + '-' + this._to2digit(month) + '-' + year;
        }
      }
  
      private _to2digit(n: number) {
          return ('00' + n).slice(-2);
      } 
  
      getFirstDayOfWeek(): number {
        return 1;
      }
  
      getDayOfWeekNames(style): string[] {
        
            const SHORT_NAMES = ['Søn', 'Man', 'Tir', 'Ons', 'Tor', 'Fre', 'Lør'];
        
            return SHORT_NAMES;
      }
    
    }
  