import { InfoDialogDialog } from './InfoDialogDialog';

import { EditBookingDialogComponent } from './edit-booking-dialog/edit-booking-dialog.component';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { BookingsTableDataSource } from './bookings-table-datasource';
import { BookingsService } from '../bookings/bookings.service';
import {Booking} from '../bookings/BookingModel';
import { DatePipe } from '@angular/common';
import { RetBookingComponent } from './ret-booking/ret-booking.component';
import { SimpleDataService } from '../simple-data.service';
import { animate, trigger, transition, style } from '@angular/animations';
import { ConfirmationDialog } from './ConfirmationDialog';


@Component({
  selector: 'bookings-table',
  templateUrl: './bookings-table.component.html',
  styleUrls: ['./bookings-table.component.css'],
  animations: [
    trigger('stepAnimation', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),
        animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))
      ]),
   transition(':leave', [  // before 2.1: transition('* => void', [
    //  style({transform: 'translateX(0%)'}),
    //  animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
   ])
  ])]
})

export class BookingsTableComponent implements OnInit {

 // @Input() bookings: Booking[];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: BookingsTableDataSource;
  downloading:boolean = false;
  debug:boolean = true;
  company_id:number;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  //displayedColumns = ['info','departure_time', 'overfart', 'ferry_name', 'Afgang', 'Ankomst','RegId','unittype_name','AntPers', 'skift' ,'ret', 'slet', 'send'];


  displayedColumns = ['info','departure_time', 'overfart',  'Afgang',  'skift' ,'ret', 'slet', 'send'];

  bookings: Booking[] = []

  temp_bookings: Booking[] = []

  constructor(private bookingsService: BookingsService, public dialog: MatDialog, private datePipe: DatePipe, public snackBar: MatSnackBar, private simpleDataService: SimpleDataService) { }

  




  site_name:string

  ngOnInit() {
    this.downloading = true
    let rootVar = window["rootVar"];

    this.simpleDataService.getSiteId(rootVar.site_name).then(result => {      
      this.company_id = result.selskabs_id
     
      this.site_name = rootVar.site_name;


       
       this.bookingsService.getCustomerBookings(this.company_id ).then(result => {   
        this.bookings = result.UserCollection
       
        this.dataSource = new BookingsTableDataSource(this.paginator, this.sort, this.bookings);
        
       this.temp_bookings = result.user_bookings_not_ready
       this.downloading = false 
       // this.bookings.sort(this.displayedColumns["departure_time"])
       this.paginator._intl.itemsPerPageLabel = 'Antal pr. side';       
       this.paginator._intl.firstPageLabel = 'Første side';
       this.paginator._intl.previousPageLabel = 'Forrige side';
       this.paginator._intl.nextPageLabel = 'Næste side';
       this.paginator._intl.lastPageLabel = 'Sidste side';
       this.paginator._intl.getRangeLabel = (page: number, pageSize: number, length: number) => {
        return  ((page * pageSize) + pageSize) + ' af ' + length;
    }
       

        
      }).catch(err => {
          console.log(err); 
      })
    })
  }

  getArrivalTime(row) {
    return new Date(row.departure_time).addMinutes(row.duration)
  }


  openInfoDialog(booking) {


    let booking_info = [
      {label: 'Booking ID', text: booking.c_bookid},
      {label: 'Fartøj', text: booking.bookings[0].ferry_name},
      {label: 'Overfart', text: booking.start_harbors.join('-') + '-' +  booking.slut_havn},
      {label: 'Dato', text:  this.datePipe.transform(booking.departure_time, 'dd-MM-yyyy')},
      {label: 'Tid', text:  this.datePipe.transform(booking.departure_time, 'HH:mm')},

      {label: 'Varighed/minutter', text: booking.duration},

      {label: 'Ankomst', text:  this.datePipe.transform(this.getArrivalTime(booking), 'HH:mm')},
      
      {label: 'Antal personer', text: booking.AntPers},
      {label: 'Antal børn', text: booking.AntalBoern},

      {label: 'RegID/Navn', text: booking.RegId},
      {label: 'Enhedstype', text: (booking.unittype_name  + ' ' + (booking.NumberOfItems > 1 ? booking.NumberOfItems : ''))},
      {label: 'RegID/Navn', text: booking.RegId},
      {label: 'Oprettet', text:  this.datePipe.transform(booking.created_at, 'dd-MM-yyyy HH:mm')},
      {label: 'Beløb', text:  booking.amount + ' ' },

      
    ]

    let dialogRef = this.dialog.open(InfoDialogDialog, {
      
      // width: '70%',
      // height: '90%',
      data: booking_info
      
    });


    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });

  }


  openRetBookingDialog(booking) {

    let dialogRef = this.dialog.open(RetBookingComponent, {
      
      // width: '70%',
      // height: '90%',
      data: booking
      
    });


    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
      // console.info(result);

      if(result) {
        
        this.bookingsService.saveMyBooking(result).then(result2 => { 

          booking.RegId = result.RegId

          this.snackBar.open("Booking", "er opdateret", {
            duration: 2000,
          });
        
        booking.bookings.forEach(element => { 
          element.RegId = result.RegId
          element.Bem = result.Bem
        });

        })
      }
    });
  } 

  openEditBookingDialog(booking) {
    console.log(booking)
    let dialogRef = this.dialog.open(EditBookingDialogComponent, {
      
      // width: '70%',
      // height: '90%',
      data: booking
      
    });


    dialogRef.afterClosed().subscribe(result => {
      


      if(result) {
        this.bookingsService.moveBooking(result).then(result => { 


          this.bookingsService.getCustomerBookings(this.company_id ).then(result => {   
              let c_bookid = booking.c_bookid
              let new_booking = result.UserCollection.filter(x => x.c_bookid == c_bookid)[0];
              let old_booking = this.bookings.find(x => x.c_bookid == c_bookid)
              let index = this.bookings.indexOf(old_booking);
            
              this.bookings[index] = new_booking;
              this.dataSource = new BookingsTableDataSource(this.paginator, this.sort, this.bookings);
              

          })

          this.snackBar.open("Booking", result, {
            duration: 2000,
          });
        })
      }


      
    });
  } 

  resendBooking(booking) {

    this.downloading = true
    let options = {
      custid: booking.bookings[0].custid,
      c_bookid: booking.c_bookid,
      site_name: this.site_name
    }          
      this.bookingsService.resendBooking(options).then(result => {       
       
        this.downloading = false;
        this.snackBar.open(result, "Ok", {
          duration: 14000,
        });
     })      
    }

    name: string;

  deleteBooking(booking: any, index: any) {
    
    const dialogRef = this.dialog.open(ConfirmationDialog, {
      width: '280px',
      data: {title:"Slet booking?",
      description: "Er du sikker på at du vil slette bookingen ? <br /><b> " + booking.start_havn + " - " + booking.slut_havn + "<br /> d. " + this.datePipe.transform(booking.departure_time, 'dd-MM-yyyy HH:mm' ) + '</b>'
    }

    });

    dialogRef.afterClosed().subscribe(result => {

      if(result == true) {

            this.downloading = true

           let options = {
              custid: booking.bookings[0].custid,
              c_bookid: booking.c_bookid,
              site_name: this.site_name
            }          
            this.bookingsService.deleteBooking(options).then(result => {       
              this.downloading = false
                this.bookings = this.bookings.filter(item => item.c_bookid !== options.c_bookid);        
                this.dataSource = new BookingsTableDataSource(this.paginator, this.sort, this.bookings);
                this.snackBar.open("Bookingen er slettet", "OK", {
                
                  duration: 2000,
                }
            
              );
            } ,error => {
              this.downloading = false
              alert('Der er opstået en fejl. Booking kan ikke slettes')
            })   
      }   
      
    });


 
  }
}
