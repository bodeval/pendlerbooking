import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetBookingComponent } from './ret-booking.component';

describe('RetBookingComponent', () => {
  let component: RetBookingComponent;
  let fixture: ComponentFixture<RetBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
