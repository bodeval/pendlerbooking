import { Component, OnInit, Inject, ViewChild, NgZone } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContent } from '@angular/material';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-ret-booking',
  templateUrl: './ret-booking.component.html',
  styleUrls: ['./ret-booking.component.css']
})
export class RetBookingComponent implements OnInit {

  b:any = null

  constructor(
    public dialogRef: MatDialogRef<RetBookingComponent>,
    @Inject(MAT_DIALOG_DATA) public booking: any,
    private ngZone: NgZone
  ) { }

  ngOnInit() {

    this.b = { 
      c_bookid: this.booking.c_bookid,
      Bem: this.booking.bookings[0].Bem,
      RegId: this.booking.RegId,
      company_id: this.booking.bookings[0].Selskabs_id

    }
  }


  @ViewChild('autosize') autosize: CdkTextareaAutosize;

  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this.ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosize.resizeToFitContent(true));
  }

  save_booking(booking) {
    this.dialogRef.close(booking);
  }
}
