import { BookingsService } from './../../bookings/bookings.service';
import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogContent } from '@angular/material';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-edit-booking-dialog',
  templateUrl: './edit-booking-dialog.component.html',
  styleUrls: ['./edit-booking-dialog.component.css']
})
export class EditBookingDialogComponent implements OnInit {

  debug = false;
  departures = []
  max_days_ahead = 40

  possible_dates = []
  startDate = new FormControl(new Date());

  minDate = new Date();
  maxDate = new Date();
  downloading:boolean = false;
 constructor(
    public dialogRef: MatDialogRef<EditBookingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public booking: any,
    private bookingsService: BookingsService,
    private datePipe: DatePipe
  ) { }



  moveBooking(departure) {   
   var options =  {
      c_bookid: this.booking.c_bookid,
      departure_ids: departure.path,
    }
  
    this.dialogRef.close(options);

  }


  

  ngOnInit() {
    this.downloading = true;
    let today = new Date()
    this.getDepartures(this.booking.departure_time, this.booking)
    this.startDate = new FormControl(this.booking.departure_time);
    this.maxDate = new Date(this.booking.departure_time).addDays(this.max_days_ahead)

    this.bookingsService.getPossibleDepartures(today, new Date(this.booking.departure_time).addDays(this.max_days_ahead), this.booking.start_havn_id, this.booking.slut_havn_id)
    .then(result =>{ 
      this.possible_dates = result      
    })

  }

  getArrivalTime(row) {
    return new Date(row.departure_time).addMinutes(row.duration)
  }


  possibleDatesFilter = (d: Date): boolean => {
    // const day = d.getDay();
    // // Prevent Saturday and Sunday from being selected.
    // return day !== 0 && day !== 6;

  //   return $.grep(departureDates, function (elm) {           
  //     return moment(date).format("DD-MM-YYYY") == moment(elm).format("DD-MM-YYYY")
  // }).length > 0    
  let result = false;
  this.possible_dates.forEach(elm => {    
    if(this.datePipe.transform(d, 'dd-MM-yyyy') == this.datePipe.transform(elm, 'dd-MM-yyyy')) {
      result = true
      return false;
    }
  })
  return result
}

  
  /***************************************SELECT DATES************************************************** */
  selectDepartureDate(type: string, event: MatDatepickerInputEvent<Date>) {

    
    this.getDepartures(event.value, this.booking)
    // this.endDateminDate = new Date(event.value.getFullYear() , event.value.getMonth(), event.value.getDate()).addDays(1);    
    // if(this.endDate.value) {
    //   if(this.endDate.value < this.startDate.value) {
    //     this.endDate.setValue(null);        
    //   }
    // }

    // if(this.step > 2) {
    //   this.onReloadDepartures()
    // }    
  }


  getDepartures(date, booking) {
    this.bookingsService.getDepartures(date, booking).then(result => { 
      this.departures = result
      this.downloading = false;
    })
  }
}

