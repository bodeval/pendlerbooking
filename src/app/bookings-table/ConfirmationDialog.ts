import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'confirme-dialog',
  templateUrl: 'confirme-dialog.html',
  styles: ['.confirm_description { padding: 12px;}']
})
export class ConfirmationDialog {
  constructor(private sanitizer: DomSanitizer, public dialogRef: MatDialogRef<ConfirmationDialog>,
    @Inject(MAT_DIALOG_DATA)
    public data: any) { 
      
    }
  onNoClick(): void {
    this.dialogRef.close();
  }

  

  close(result): void {
    this.dialogRef.close(result);
    
  }
}