import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
@Component({
  selector: 'info-dialog',
  templateUrl: 'info-dialog.html',
})
export class InfoDialogDialog {
//   constructor(
//     @Inject(MAT_DIALOG_DATA)
//     public data: any) { }


    constructor(
        public dialogRef: MatDialogRef<InfoDialogDialog>,
        @Inject(MAT_DIALOG_DATA) public booking: any
      ) { }
}