import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerInfoComponentComponent } from './customer-info-component.component';

describe('CustomerInfoComponentComponent', () => {
  let component: CustomerInfoComponentComponent;
  let fixture: ComponentFixture<CustomerInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
