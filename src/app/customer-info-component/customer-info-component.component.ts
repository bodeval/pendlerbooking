import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-info-component',
  templateUrl: './customer-info-component.component.html',
  styleUrls: ['./customer-info-component.component.css']
})
export class CustomerInfoComponentComponent implements OnInit {

 // rootVar:any
  info:any
  titel:string = "Kundeoplysninger"
  constructor() { }

  ngOnInit() {
    let rootVar = window["rootVar"];

    this.info = [{
      label: 'Telefon',
      value: rootVar.customer_phone

    },
    {
      label: 'Navn',
      value: rootVar.customer_name

    },
    {
      label: 'Adresse',
      value: rootVar.Address

    },
    {
      label: 'Postnummer',
      value: rootVar.Zip

    },
    {
      label: 'By',
      value: rootVar.city

    },
    {
      label: 'Email',
      value: rootVar.Email

    },
    {
      label: 'Mobil',
      value: rootVar.Mobile

    }
    ,
    {
      label: 'Nummerplade',
      value: rootVar.LicensePlateId

    }]
  }

}






  