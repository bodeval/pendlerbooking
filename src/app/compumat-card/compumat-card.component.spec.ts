import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompumatCardComponent } from './compumat-card.component';

describe('CompumatCardComponent', () => {
  let component: CompumatCardComponent;
  let fixture: ComponentFixture<CompumatCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompumatCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompumatCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
