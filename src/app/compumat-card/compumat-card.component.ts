import { Component, OnInit } from '@angular/core';
import {URLSearchParams} from '@angular/http';
import {PlatformLocation, DatePipe } from '@angular/common';

@Component({
  selector: 'app-compumat-card',
  templateUrl: './compumat-card.component.html',
  styleUrls: ['./compumat-card.component.css']
})
export class CompumatCardComponent implements OnInit {

  CompumatCards:any
  customer_id:number
  title:string = 'Værdi på kort'
  constructor(private platformLocation: PlatformLocation, private datePipe: DatePipe) { 
    
  }

  ngOnInit() {

    let rootVar = window["rootVar"];

    let str_card = rootVar.CompumatCards

    this.customer_id = rootVar.customer_id

    this.CompumatCards = rootVar.CompumatCards
  }

  tankOplLink(card) {

    let origin = (this.platformLocation as any).location.origin

    let todate = new Date();
    todate.setDate( todate.getDate() + 1 );

    var options = {
      action: 'CreateOrder',
      Keyno: card.KeyNo,
      Custid:  this.customer_id,
      callbackurl: origin, 
      deleteorder: origin,
      bookings: 'Header:indsæt på kort;datefrom:' + this.datePipe.transform(new Date(), 'dd.MM.yy')  + ';dateto:' + this.datePipe.transform(todate, 'dd.MM.yy')+';profileid:13;unitid:!SKYGGE!;VARS:v_1'    
    }

    let params = new URLSearchParams();
    for(let key in options){
        params.set(key, options[key]) 
    }

    return origin + '/default.aspx?' + params
  }

 
    
}
