import { StorageServiceModule } from 'angular-webstorage-service';
import {Inject, Injectable } from '@angular/core';

import { SESSION_STORAGE, StorageService} from 'angular-webstorage-service';
import { InjectableAnimationEngine } from '@angular/platform-browser/animations/src/providers';

const STORAGE_KEY = 'local_params';

// @Injectable({
//   providedIn: 'root'
// })


@Injectable()


export class LocaleStorageService {

  anotherParams = {};

  constructor(@Inject(SESSION_STORAGE) private storage: StorageService) { }


      //https://medium.com/@tiagovalverde/how-to-save-your-app-state-in-localstorage-with-angular-ce3f49362e31


      public storeOnLocalStorage(params: any): void {
        //get array of tasks from local storage
        const currentTodoList = this.storage.get(STORAGE_KEY) || [];
        // push new task to array
        // currentTodoList.push({
        //   title: taskTitle,
        //   isChecked: false
        // });
        // insert updated array to local storage
        this.storage.set(STORAGE_KEY, params);
        console.log(this.storage
          .get(STORAGE_KEY) || 'LocaL storage is empty');
      }

      public getOnLocalStorage(): any {
        return this.storage.get(STORAGE_KEY) || 'LocaL storage is empty';
      }
}





