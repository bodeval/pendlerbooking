import { Component, OnInit } from '@angular/core';
import { BookingsService } from './bookings.service';
import { SimpleDataService } from '../simple-data.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

  title = "Mine bookinger"

  customer_name:string
  customer_phone:string
  customer_id:string
  company_id:number
  site_name:string

  downloading:boolean = false;
  
  bookings = []
  
  constructor(private bookingsService: BookingsService, private simpleDataService: SimpleDataService) { }

  ngOnInit(): void {    
    let rootVar = window["rootVar"];
    this.downloading = true

    this.customer_name = rootVar.customer_name
    this.customer_phone = rootVar.customer_phone
    this.customer_id = rootVar.customer_id
    this.company_id = rootVar.company_id
    this.site_name = rootVar.site_name
    
   
      
      this.simpleDataService.getSiteId(rootVar.site_name).then(result => {      
        this.company_id = result.selskabs_id
        rootVar.company_id = result.selskabs_id
        
        this.bookingsService.company_id = result.selskabs_id
        
        this.bookingsService.getCustomerBookings(result.selskabs_id).then(result => {   
          this.bookings =  result.UserCollection
        }).catch(err => {
            console.log(err); 
        })
      })
   

   
   
    

    

    
  }

 
}
