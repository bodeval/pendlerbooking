import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpClient} from '@angular/common/http';
import { Headers, Http } from '@angular/http';
import { Booking } from './BookingModel';
import { Departure } from '../models';

@Injectable({
  providedIn: 'root'
})
export class BookingsService {

  base_url:string
  customer_id:number
  company_id:number
  site_id:string

  constructor(private http: Http,  private datePipe: DatePipe) {
    let rootVar = window["rootVar"];    
    this.customer_id =  rootVar.customer_id
    this.base_url = rootVar.base_url
    this.company_id = rootVar.company_id
    this.site_id = rootVar.site_name; 
  }



  getCustomerBookings(company_id): Promise<any> {  
    
    return this.http.get(this.base_url +'/api/Diverse/GetCustomerBookings?customer_id=' + this.customer_id + '&company_id=' + company_id)
               .toPromise()
               .then(response => 
                response.json() as any )
               .catch(this.handleError);
  }

 





  getPossibleDepartures(start_date, end_date, departure_harbor_id, arrival_harbor_id): Promise<any>{
    let str_start_date = this.datePipe.transform(start_date, 'dd-MM-yyyy')
    let str_end_date = this.datePipe.transform(end_date, 'dd-MM-yyyy')

    return this.http.get(this.base_url +'/api/Diverse/GetPossibleDepartures?str_start_date=' + str_start_date + '&str_end_date=' + str_end_date + '&departure_harbor_id=' + departure_harbor_id + '&arrival_harbor_id=' + arrival_harbor_id)
               .toPromise()
               .then(response => 
                response.json() as any )
               .catch(this.handleError);

  }


  getDepartures(start_date, booking): Promise<Departure[]> {    
    console.log(booking)
    let options = {

    
      str_start_date: this.datePipe.transform(start_date, 'dd-MM-yyyy'),
      str_end_date: this.datePipe.transform(start_date, 'dd-MM-yyyy'),
      site_id:  this.company_id,
      NoOfPersons: booking.AntPers,
      NoOfAdults: booking.bookings[0].AntalVoksne,
      NoOfChildren: booking.AntalBoern,
      NoOfIslanders: 0,
      NumberOfItems: booking.bookings[0].NumberOfItems,  
      Length: booking.bookings[0].Langde,
      NoOfUnits: booking.bookings[0].AntEnh,
      Height: booking.bookings[0].Hoj,
      Weight: booking.bookings[0].Vagt,
      WeightCargo: booking.bookings[0].VaegtFragt,
      DepartureHarborId: booking.start_havn_id,
      ArrivalHarborId: booking.slut_havn_id,
      UnitTypeId: booking.unittype_id,                
      
    }
    
    return this.http.post(this.base_url + '/api/Pendlerbooking/GetDepartures', options
          )
               .toPromise()
               .then(response => response.json() as Departure[])
               .catch(this.handleError);
  }


  resendBooking(options): Promise<any> {
    console.log(options)
    return this.http.post(this.base_url + '/api/Booking/ResendBooking', options)
       .toPromise()
       .then(response => response.json() as any)
       .catch(this.handleError);
  }


  moveBooking(options): Promise<any> {
    
    options.site_id = this.site_id
    options.company_id = this.company_id
    options.customer_id = this.customer_id
    console.log(options)
    return this.http.post(this.base_url + '/api/Booking/MoveBooking', options)
       .toPromise()
       .then(response => response.json() as any)
       .catch(this.handleError);
  }

  deleteBooking(options): Promise<any> {
    
    return this.http.post(this.base_url + '/api/Booking/DeleteBooking', options)
       .toPromise()
       .then(response => response.json() as any)
       .catch(this.handleError);
  }


  
  saveMyBooking(options): Promise<any> {
    
    options.site_name = this.site_id
    options.customer_id = this.customer_id
    options.company_id = this.company_id

    return this.http.post(this.base_url + '/api/Pendlerbooking/SaveMyBooking', options)
       .toPromise()
       .then(response => response.json() as any)
       .catch(this.handleError);
  }
 
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); 
    return Promise.reject(error.message || error);
  }

}
