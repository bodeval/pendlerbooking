import { Component, OnInit, Input,Output, OnChanges, EventEmitter,ViewChild } from '@angular/core';
import { UnitType, BookingOptions, PersonType } from '../models';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms'
import { SimpleDataService } from '../simple-data.service';
import {LocaleStorageService } from '../locale-storage.service';
@Component({
  selector: 'app-unittype-form',
  templateUrl: './unittype-form.component.html',
  styleUrls: ['./unittype-form.component.css']
})
export class UnittypeFormComponent implements OnChanges, OnInit {
  @ViewChild('optionForm') form;



  constructor(private simpleDataService: SimpleDataService,  private localStorage: LocaleStorageService ) {

  }

  @Input() step: number
  @Input() unit_type: UnitType;
  @Input() person_types: PersonType[];
  @Input() downloading: boolean

  @Output() onOptionsSubmit = new EventEmitter < any > ();
  @Input() options: BookingOptions

  customer_type: string

  ngOnInit() {
    
    let rootVar = window["rootVar"];
    this.customer_type = rootVar.customer_type
  }

  init_unitType() {
    
    this.unit_type.person_types = [];
    if (this.unit_type.limits != "") {
      if (typeof this.unit_type.limits == 'string') {
        let limits = JSON.parse(this.unit_type.limits)
        this.unit_type.limits = limits
      } else {
        this.unit_type.limits = {}
      }

    } else {

      this.unit_type.limits = {}
    }



    if (this.unit_type.person_type_ids) {

      let unit_type_personType_ids = this.unit_type.person_type_ids.split(',').map(Number);
      this.person_types.forEach((element, index) => {
        if (unit_type_personType_ids.indexOf(element.id) > -1) {
          
          let params:any = this.localStorage.getOnLocalStorage()
          //console.log(params)
          if (this.unit_type.person_types.length == 0) {
            element.number_of_persons = 1
          } else {
            element.number_of_persons = 0
          }
    
          if(!params.params ) {
          
           
          } else {
            this.options.RegID = params.params.options.RegID

            this.options.Weight = params.params.options.Weight

            this.options.WeightCargo = params.params.options.WeightCargo

            this.options.Length = params.params.options.Length

            this.options.Height = params.params.options.Height

            
            
            
            
          }

          this.unit_type.person_types.push(Object.assign({}, element))
        }
      });
      
    }


  }

  checkMinPerson() {

    if(this.unit_type.KravMinimumEnPerson) {
      
      this.unit_type.person_types
      let number_of_persons = 0
      this.unit_type.person_types.forEach(element =>{  
        number_of_persons += element.number_of_persons;
      });
      
      return number_of_persons > 0;
    } else {
      return true;
    }
  }


  ngOnChanges() {

    if (!this.options.is_set) {
      this.init_unitType()
      this.form.reset();
    }

    //this.options = new BookingOptions();
    if (!this.unit_type.KravLangde) {
      this.options.Length = this.unit_type.StdLangde
    }

    if (!this.unit_type.AngivAntal) {
      this.options.NoOfUnits = this.unit_type.EnhStor
    }

    if (!this.unit_type.Hoj) {
      this.options.Height = 1
    }

    if (!this.unit_type.KravVagt) {
      this.options.Weight = this.unit_type.StdVagt
    }

    if (!this.unit_type.KravFragtVaegt) {
      this.options.WeightCargo = 0;
    }

    this.options.NumberOfItems = 1;
  }

  onSubmit() {

    if (this.unit_type.KravLangde) {
      this.options.NoOfUnits = this.unit_type.StdLangde * this.options.Length / this.unit_type.EnhStor
    }

    if (!this.unit_type.KravVagt) {
      this.options.Weight = this.unit_type.StdVagt;
    }

    if (!this.unit_type.KravFragtVaegt) {
      this.options.WeightCargo = 0;
    }

    this.options.Height = 1;
    this.options.PersonTypes = this.unit_type.person_types;

    this.options.is_set = true;

    

    this.localStorage.storeOnLocalStorage({params: { options: this.options, unit_type: this.unit_type}})
    this.onOptionsSubmit.emit(this.options)
  }

  getSelectValues(limits): number[] {


    limits.default = parseFloat(limits.default);
    let result = [];
    let i = parseFloat(limits.min);
    let step = parseFloat(limits.steps.replace(",", "."))
    let max = parseFloat(limits.max)
    while (i <= max) {

      result.push(i)
      i += step
    }
    return result;
  }

  parseToFloat(str) {
    return parseFloat(str);
  }

  visible_by_customer_type(types) {
    let result = true

    if (types != "") {
      if (this.customer_type != "") {
        let types_array = types.split(",")
        if (types_array.indexOf(this.customer_type) == -1) {
          result = false
        }
      }
    }
    return result;
  }
}
