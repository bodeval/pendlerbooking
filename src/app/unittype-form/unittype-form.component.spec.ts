import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnittypeFormComponent } from './unittype-form.component';

describe('UnittypeFormComponent', () => {
  let component: UnittypeFormComponent;
  let fixture: ComponentFixture<UnittypeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnittypeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnittypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
