import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import 'hammerjs';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));


  declare global {
    interface Date {
      addMinutes: (minutes: number) => Date;
    }
  }
    (<any>Date.prototype).addMinutes = function (minutes) {
      var copiedDate = new Date(this.getTime());
      return new Date(copiedDate.getTime() + minutes * 60000);
    }
   

    declare global {
    interface Date {
      addDays: (days: number) => Date;
    }
  }
    (<any>Date.prototype).addDays = function (days) {
      var copiedDate = new Date(this.getTime());
      return new Date(copiedDate.getTime() + (days * 60000 * 24 * 60));  //24 * 60 * 60000)
    
    }
  
  
    
    
  