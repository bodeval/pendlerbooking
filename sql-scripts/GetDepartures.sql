/****** Script for SelectTopNRows command from SSMS  ******/
declare @str_date varchar(30) = '16-12-2016'

declare @str_end_date varchar(30) = '20-12-2017'
declare @selskab_id int = 1
declare @rute varchar(30) = '1'
SELECT convert(varchar(30), convert(date,a.Datotid)) as dato,
	 a.Overfartstid, a.Afgangs_id, a.DatoTid, o.Navn, o.Overfart_id, o.Rute_Id, Sh.Navn as starthavn , uh.Navn as sluthavn , a.Fartoj_id
	, O.StartHavn_Id as starthavn_id
	, O.SlutHavn_Id as sluthavn_id
	, F.Navn as ferry_name
	, A.BookSelvProcent
	, EnhedsTypeBookSelvPct
	, PersonBookSelvPct
	, tonnage
	, Akt_lukketid_IfBookings
	, Udsolgt
	, Locked
	, Akt_lukketid  
	, 

	F.MaxDakLang as MaxLength, F.MaxDakHLang as MaxHeight, F.MaxEnh as MaxUnitsAmount, A.MaxPers, 
				ISNULL(A.Tonnage, F.MaxVagt) as MaxWeight, 
				ISNULL((SELECT sum(Vagt) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumWeight, 
				ISNULL((SELECT sum(AntPers) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumPersons,
				ISNULL((SELECT sum(AntEnh) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumUnitsAmount,
				ISNULL((SELECT sum(Langde) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumLength, 
				ISNULL((SELECT sum(AntalVoksne) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumAdults, 
				ISNULL((SELECT sum(AntalBoern) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumChildren, 
				ISNULL((SELECT sum(Antaloeboer) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumIslanders ,
				ISNULL((SELECT count(*) FROM Bookinger where  Afgangs_id = a.afgangs_id),0) as SumBookings ,

    isnull( 
	(
		select top 1 ia.Afgangs_id From afgange IA
		inner join overfarter IO on  io.Overfart_id = IA.Overfart_id
		where iO.StartHavn_Id = O.SlutHavn_Id and IA.DatoTid > A.DatoTid  
		and convert(date,IA.DatoTid)  = convert(date,a.DatoTid)
		and IA.Fartoj_id = a.Fartoj_id
			
		order by IA.DatoTid
		)
, 0) as naeste

FROM [Afgange] A
inner join Overfarter o on o.Overfart_id = A.Overfart_id
inner join Havne SH on SH.havne_id = O.StartHavn_Id
Inner join Fartojer F on F.Fartoj_id = A.Fartoj_id

inner join Havne UH on UH.havne_id = O.SlutHavn_Id

where   convert(date,a.Datotid) >= convert(date,@str_date)
	and convert(date,a.Datotid) <= convert(date,@str_end_date)	
	and a.selskabs_id = @selskab_id
	and a.Fartoj_id in (
		--SELECT Fartoj_id  FROM Driftsplan dp  WHERE Selskabs_Id= @selskab_id  AND (convert(date,@str_date) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)) And Rute_id = @rute)
		SELECT Fartoj_id  FROM Driftsplan dp  WHERE Selskabs_Id= @selskab_id  AND (convert(date,a.Datotid) BETWEEN convert(date,dp.Start_Dato) AND convert(date,dp.Slut_Dato)) And Rute_id = @rute)
order by a.DatoTid
                              